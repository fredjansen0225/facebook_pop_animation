//
//  AppDelegate.m
//  TestApp
//
//  Created by soft on 29/01/15.
//  Copyright (c) 2015 soft. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"



@interface AppDelegate ()

@end




@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil] ;
    ViewController *root = [storyboard instantiateInitialViewController];
    
    self.window.rootViewController = root;
    [self.window makeKeyAndVisible];
    self.delegate = root;
    
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (notification != nil) {
        [self.delegate showNotification:notification];
    }
    
    return YES;
}



-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
   [self application:application notification:notification];
}



- (void)application:(UIApplication *)application notification:(UILocalNotification *)notification
{
    
    if (notification) {
        
    
        ViewController* masterCtrl = (ViewController*)self.window.rootViewController;
    
        if([masterCtrl isKindOfClass: [ViewController class]]  )
        {
        
            [masterCtrl showNotification:notification];
        //[navCtrl popToViewController:masterCtrl animated:YES];
        }
        else
        {
            ViewController* masterCtrl = [[ViewController alloc] init];
            [masterCtrl showNotification:notification];
       // [navCtrl popToViewController:masterCtrl animated:YES];
        }
    }
    
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
