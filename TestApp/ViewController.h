//
//  ViewController.h
//  TestApp
//
//  Created by soft on 29/01/15.
//  Copyright (c) 2015 soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <pop/POP.h>
#import "AppDelegate.h"
#import "FXBlurView.h"

@interface ViewController : UIViewController<POPAnimatorDelegate, UITextFieldDelegate, ShowNotificationDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel1;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel2;

@property (weak, nonatomic) IBOutlet UITextField *goalTextField;

@property (weak, nonatomic) IBOutlet UILabel *coolLabel;

@property (weak, nonatomic) IBOutlet FXBlurView *ovalView;
@property (weak, nonatomic) IBOutlet UIImageView *ovalImage;
@property (weak, nonatomic) IBOutlet UILabel *ovalTitle;
@property (weak, nonatomic) IBOutlet UIButton *ovalThanksButton;
@property (weak, nonatomic) IBOutlet UIImageView *ovalTransbg;


@property (weak, nonatomic) IBOutlet UIView *yesOrNoView;
@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet UIImageView *divider;

@end

