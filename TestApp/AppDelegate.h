//
//  AppDelegate.h
//  TestApp
//
//  Created by soft on 29/01/15.
//  Copyright (c) 2015 soft. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ShowNotificationDelegate <NSObject>

- (void) showNotification:(UILocalNotification*) notification;

@end


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) id<ShowNotificationDelegate> delegate;

@end

