//
//  ViewController.m
//  TestApp
//
//  Created by soft on 29/01/15.
//  Copyright (c) 2015 soft. All rights reserved.
//

#import "ViewController.h"
#import <UIImage+BlurredFrame.h>
#import <UIImage+ImageEffects.h>

@interface ViewController ()
{
    POPBasicAnimation *animForTitle1, *animForTitle2, *animForGoalText, *animForCoolLabel;
    BOOL achieved;
}
@end

@implementation ViewController

@synthesize  titleLabel1,titleLabel2,coolLabel,ovalView,yesOrNoView,goalTextField,ovalImage,ovalThanksButton,ovalTitle,ovalTransbg,yesButton,noButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self registerAcheiveNotification];
    [goalTextField setTintColor:[UIColor whiteColor]];
    
    
    //configure blur view
    self.ovalView.dynamic = NO;
    self.ovalView.tintColor = [UIColor colorWithRed:0.4 green:0.5 blue:0.5 alpha:1];
    self.ovalView.contentMode = UIViewContentModeBottom;
    ovalView.blurEnabled = NO;

}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initViews];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
    NSString *goal = [userDefault valueForKey:@"Goal"];
    
    if (goal) {
        [self startAchievementAnimation];
        
    }
    else
        [self startIntroAnimation];

    
    
}

- (void) initViews
{
    [self initTitles];
    [self initGoalFieldAndCoolLabel];
    [self initOvalView];
    [self initYesOrNoView];
}


- (void) initTitles
{
    [self.titleLabel1 setHidden:YES];
    titleLabel1.alpha = 0;
    NSDate * date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEEE"];
    
    //Optionally for time zone conversions
    NSString *weekday = [formatter stringFromDate:date];
    NSString *title1 = [NSString stringWithFormat:@"On this beautiful %@", weekday];
    titleLabel1.text = title1;
    
    [self.titleLabel2 setHidden:YES];
    titleLabel2.alpha = 0;

}

-(void) initGoalFieldAndCoolLabel
{
    [goalTextField resignFirstResponder];
    [self.goalTextField setHidden:YES];
    goalTextField.alpha = 0;
    
    
    [self.coolLabel setHidden:YES];
    coolLabel.alpha = 0;

}

- (void) initOvalView
{
    [self.ovalView setHidden:YES];
    ovalTitle.alpha = 0;
    ovalThanksButton.alpha = 0;
    ovalThanksButton.layer.cornerRadius = 15;
    ovalThanksButton.layer.borderWidth = 2  ;

}

-(void) initYesOrNoView
{
    [self.yesOrNoView setHidden:YES];
    yesOrNoView.alpha = 0;
    yesButton.alpha = 0;
    noButton.alpha = 0;
    self.divider.alpha = 1;

}

- (void) startIntroAnimation
{
    [self showTitle1];
   
}

- (void) startAchievementAnimation
{
    [self showYesOrNoView];
}


- (IBAction)yesButtonClick:(id)sender {
    [self showOvalViewWithYes:YES];
    achieved = YES;
}

- (IBAction)noButtonClick:(id)sender {
    [self showOvalViewWithYes:NO];
    achieved = NO;
}

- (IBAction)thanksButtonClick:(id)sender {
    
    [self hideOvalView];
    //[self.yesOrNoView setHidden:YES];

    self.divider.alpha = 0;
    if (achieved) {
        noButton.alpha = 0;
        
        
        POPDecayAnimation *anim = [POPDecayAnimation animationWithPropertyNamed:kPOPLayerPositionX];
        anim.velocity = @(150.);
        [yesButton.layer pop_addAnimation:anim forKey:@"slide"];
        
    }
    else{
        POPDecayAnimation *anim = [POPDecayAnimation animationWithPropertyNamed:kPOPLayerPositionX];
        anim.velocity = @(-150.);
        [noButton.layer pop_addAnimation:anim forKey:@"slide"];
        yesButton.alpha = 0;
    }
    
}


#pragma animation functions start
- (void) showTitle1
{
    animForTitle1 = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    animForTitle1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animForTitle1.name = @"AnimationForTitle1";
    animForTitle1.fromValue = @(0.0);
    animForTitle1.toValue = @(1.0);
    animForTitle1.duration = 1;
    animForTitle1.delegate = self;
    [animForTitle1 setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        [self showTitle2];
        
    }];
    [titleLabel1 setHidden:NO];
    [titleLabel1 pop_addAnimation:animForTitle1 forKey:@"fadeForTitle1"];
}
- (void) showTitle2
{
    
    
    animForTitle2 = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    animForTitle2.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animForTitle2.fromValue = @(0.0);
    animForTitle2.toValue = @(1.0);
    animForTitle2.duration = 1;
    //animForTitle2.beginTime = CACurrentMediaTime() +  animForTitle1.duration;
    animForTitle2.delegate = self;
    [animForTitle2 setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        [self showGoalField];
    }];
    [titleLabel2 setHidden:NO];
    [titleLabel2 pop_addAnimation:animForTitle2 forKey:@"faceForTitle2"];
}
- (void) showGoalField
{
    animForGoalText = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    animForGoalText.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animForGoalText.fromValue = @(0.0);
    animForGoalText.toValue = @(1.0);
    animForGoalText.duration = 1;
    animForGoalText.delegate = self;
    
    [animForGoalText setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        
        [goalTextField becomeFirstResponder];
    }];
    [goalTextField setHidden:NO];
    [goalTextField pop_addAnimation:animForGoalText forKey:@"faceForGoal"];
}
- (void) showCoolLabel
{
    animForCoolLabel = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    animForCoolLabel.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animForCoolLabel.fromValue = @(0.0);
    animForCoolLabel.toValue = @(1.0);
    animForCoolLabel.duration = 1;
    animForCoolLabel.delegate = self;
    animForCoolLabel.beginTime = CACurrentMediaTime() + 0.3;
    
    [animForCoolLabel setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        [self saveGoal];
    }];

    [coolLabel setHidden:NO];
    [coolLabel pop_addAnimation:animForCoolLabel forKey:@"fadeForCool"];
}

- (void) showOvalViewWithYes:(BOOL)done
{
    [ovalView setHidden:NO];
//    UIImage *trans = [UIImage imageNamed:@"no_icon"];
//    trans = [trans applyLightEffect];
//    //trans = [trans applyBlurWithRadius:30.0 tintColor:[UIColor whiteColor] saturationDeltaFactor:10 maskImage:trans];
//    ovalTransbg.image = trans;
//    [ovalTransbg setHidden:YES];
//    ovalImage.alpha = 0;
//    
//    
//    UIBlurEffect * effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
//    UIVisualEffectView * viewWithBlurredBackground =
//    [[UIVisualEffectView alloc] initWithEffect:effect];
//    
//    [ovalView addSubview:viewWithBlurredBackground];
    
    
    if (done)
    {
        ovalTitle.text = @"You're awesome! We \n knew you had it in you.";
        ovalTitle.textColor =[UIColor colorWithRed:89.0/255 green:136.0/225 blue:93.0/225 alpha:1];
        
        ovalThanksButton.layer.borderColor = [[UIColor colorWithRed:140.0/255 green:195.0/225 blue:144.0/225 alpha:1] CGColor];
        [ovalThanksButton setTitle:@"Thanks." forState:UIControlStateNormal];
        [ovalThanksButton setTitleColor:[UIColor colorWithRed:140.0/255 green:195.0/225 blue:144.0/225 alpha:1] forState:UIControlStateNormal] ;
        ovalImage.image = [UIImage imageNamed:@"img_oval_green"];
        
        
        NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault removeObjectForKey:@"Goal"];
        
        UIApplication *app = [UIApplication sharedApplication];
        NSArray *eventArray = [app scheduledLocalNotifications];
        for (int i=0; i<[eventArray count]; i++)
        {
            UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
            NSString *alertBody = oneEvent.alertBody;
            if ([alertBody hasPrefix:@"Did you achieve"])
            {
                [app cancelLocalNotification:oneEvent];
                break;
            }
        }
        
    }
    else
    {
        ovalTitle.text = @"Ah, damn it! Oh well, you \n can always try again\n tomorrow";
        ovalTitle.textColor =[UIColor colorWithRed:195.0/255 green:151.0/225 blue:151.0/225 alpha:1];
        ovalThanksButton.layer.borderColor = [[UIColor colorWithRed:195.0/255 green:151.0/225 blue:151.0/225 alpha:1] CGColor];
        [ovalThanksButton setTitle:@"Yep." forState:UIControlStateNormal];
        [ovalThanksButton setTitleColor:[UIColor colorWithRed:195.0/255 green:151.0/225 blue:151.0/225 alpha:1] forState:UIControlStateNormal] ;
        ovalImage.image = [UIImage imageNamed:@"img_oval_red"];
    }
    
    POPBasicAnimation* animFade = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    animFade.toValue = @(1.0);
    animFade.duration = 1;
    [animFade setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        //[self saveGoal];
    }];
    [ovalImage pop_addAnimation:animFade forKey:@"fade"];
    
    POPSpringAnimation *sprintAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    sprintAnimation.fromValue = [NSValue valueWithCGPoint:CGPointMake(.4, .4)];
    sprintAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1.1, 1.1)];
    sprintAnimation.velocity = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    sprintAnimation.springBounciness = 20.f;
    [sprintAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        if (finished) {
            [self showOvalTitleAndButton];
        }
     }];
    [ovalImage pop_addAnimation:sprintAnimation forKey:@"springAnimation"];
}

- (void) hideOvalView
{

    ovalThanksButton.alpha = 0;
    ovalTitle.alpha  = 0;
    
    
    POPBasicAnimation* animFade = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    animFade.toValue = @(.0);
    animFade.duration = 1;
    [animFade setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        //[self saveGoal];
    }];
    [ovalImage pop_addAnimation:animFade forKey:@"fade"];
    
    POPSpringAnimation *sprintAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    sprintAnimation.fromValue = [NSValue valueWithCGPoint:CGPointMake(1.1, 1.1)];
    sprintAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(.4, .4)];
    sprintAnimation.velocity = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    sprintAnimation.springBounciness = 20.f;
    [sprintAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        if (finished) {
            [self initOvalView];
        }
    }];
    [ovalImage pop_addAnimation:sprintAnimation forKey:@"springAnimation"];
}


-(void) showOvalTitleAndButton
{
    POPBasicAnimation* animFade1 = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    animFade1.toValue = @(1.0);
    animFade1.duration = 0.5;
    [ovalTitle pop_addAnimation:animFade1 forKey:@"fade"];
    
    POPBasicAnimation* animFade2 = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    animFade2.toValue = @(1.0);
    animFade2.beginTime = CACurrentMediaTime() + animFade1.duration;
    animFade2.duration = 0.5;
    [ovalThanksButton pop_addAnimation:animFade2 forKey:@"fade"];
}


- (void) showYesOrNoView
{
    [self.yesOrNoView setHidden:NO];
    yesOrNoView.alpha = 1;
    
    POPBasicAnimation* animFade1 = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    animFade1.toValue = @(1.0);
    animFade1.duration = 0.5;
    [yesButton pop_addAnimation:animFade1 forKey:@"fade"];
    
    POPBasicAnimation* animFade2 = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    animFade2.toValue = @(1.0);
    animFade2.beginTime = CACurrentMediaTime() + animFade1.duration;
    animFade2.duration = 0.5;
    [noButton pop_addAnimation:animFade2 forKey:@"fade"];
    
    
    POPSpringAnimation *sprintAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    sprintAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(0.9, 0.9)];
    sprintAnimation.velocity = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    sprintAnimation.springBounciness = 20.f;
    [yesButton pop_addAnimation:sprintAnimation forKey:@"springAnimation"];
    
    POPSpringAnimation *sprintAnimation2 = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    sprintAnimation2.toValue = [NSValue valueWithCGPoint:CGPointMake(0.9, 0.9)];
    sprintAnimation2.velocity = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    sprintAnimation2.springBounciness = 20.f;
    sprintAnimation2.beginTime = CACurrentMediaTime() + animFade1.duration;
    [noButton pop_addAnimation:sprintAnimation2 forKey:@"springAnimation"];
    
}


#pragma animation functions end


-(UILocalNotification*) makeAcheivementNotification
{
    NSDate *oldDate = [NSDate date]; // Or however you get it.
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:oldDate];
    comps.hour   = 9;
    comps.minute = 0;
    comps.second = 0;
    NSDate *newDate = [calendar dateFromComponents:comps];
    
    UILocalNotification* alarmNotification = [[UILocalNotification alloc] init];
    alarmNotification.fireDate = newDate;
    alarmNotification.alertBody = @"What is the one thing you want to achieve today?";
    alarmNotification.alertAction = @"Enter goal for today.";
    //alarmNotification.repeatInterval = kCFCalendarUnitDay;
    //alarmNotification.repeatInterval = kCFCalendarUnitMinute;
    return alarmNotification;
}


-(UILocalNotification*) makeReplyNotificationWithGoal:(NSString*) goal
{
    NSDate *oldDate = [NSDate date]; // Or however you get it.
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute |NSCalendarUnitSecond;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:oldDate];
    comps.minute = comps.minute + 2;
    NSDate *newDate = [calendar dateFromComponents:comps];
    
    UILocalNotification* alarmNotification = [[UILocalNotification alloc] init];
    alarmNotification.fireDate = newDate;
    alarmNotification.alertBody = [NSString stringWithFormat: @"Did you achieve %@ for today?", goal ];
    alarmNotification.alertAction = @"Reply";
    return alarmNotification;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == goalTextField) {
        [textField resignFirstResponder];
        [self showCoolLabel];
    }
    return YES;
}
- (void) saveGoal
{
    NSString *goal = goalTextField.text;
    
    if (![goal isEqualToString:@""]) {
        UILocalNotification *noti = [self makeReplyNotificationWithGoal:goal];
        [[UIApplication sharedApplication]scheduleLocalNotification:noti];
        
        NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setValue:goal forKey:@"Goal"];
    }
    
}

- (void) registerAcheiveNotification
{
    NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
    BOOL isRegistered = [userDefault boolForKey:@"isRegistered"];
    if (!isRegistered) {
        
        UILocalNotification *notification = [self makeAcheivementNotification];
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        
        [userDefault setBool:YES forKey:@"isRegistered"];
    }
}

-(void) showNotification:(UILocalNotification*) notification
{
    if (notification == nil) {
        return;
    }
  
    NSLog(@"Notification: %@",notification.alertBody);
    if ([notification.alertBody isEqualToString:@"What is the one thing you want to achieve today?"])
    {
        [self initViews];
        [self startIntroAnimation];
    }
    else if([notification.alertBody hasPrefix:@"Did you achieve"])
    {
        [self initViews];
        [self startAchievementAnimation];
    }
}

-(void)animationDidStart:(CAAnimation *)anim
{
    NSLog(@"1");
}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    NSLog(@"2");
}
-(void)animatorDidAnimate:(POPAnimator *)animator
{
    NSLog(@"3");
}

-(void)animatorWillAnimate:(POPAnimator *)animator
{
    NSLog(@"4");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
